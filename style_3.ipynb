{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "toc-hr-collapsed": false
   },
   "source": [
    "# Coding style (III)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "... continues [style_2.ipynb](style_2.ipynb)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"tableofcontents\">*Table of contents*</a>\n",
    "\n",
    "- [Code structure](#Code-structure)\n",
    "  - [DRY](#DRY)\n",
    "  - [One function for each job](#One-function-to-rule-them-all-for-each-job)\n",
    "  - [Cyclomatic complexity](#Cyclomatic-complexity)\n",
    "  - [State, pure functions, testability](#State,-pure-functions,-testability)\n",
    "  - [Sorting into modules](#Sorting-into-modules)\n",
    "  - [Using modules as command line tools](#Using-modules-as-command-line-tools)\n",
    "    - [Doing it the hard way](#Doing-it-the-hard-way)\n",
    "    - [Using argparse](#Using-argparse)\n",
    "    - [Exercise](#excs)\n",
    "  - [Making an installable package](#Making-an-installable-package)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Code structure"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Another pillar of code clarity, and the last point I want to address regarding style, is a good structure. Good structure will not only increase the clarity of your code, but it will also make your code more reusability and better testable. This can save you a lot of pain.\n",
    "\n",
    "Consider the following scenario:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "> Bob wants to write a script that performs some typical data evaluation: loading and tidying some raw data, processing the tidy data, and plotting the results. Because it seems to be the easiest solution, Bob writes just a sequential set of instructions. Some operations have to be repeated, so he copies and pastes the code.\n",
    "Having finished the script, Bob wants to make sure that it does what he intends it to do, so he tests it by executing it line-by-line and inspects some intermediate results. At first, all seems fine, but later he finds an error somewhere close to the beginning of the script. Unfortunately, this error is in a code snippet that he copied several times, so he has to go through the code and needs to find and fix every instance of that particular snippet. Afterwards he goes through the script once more, line-by-line, printing and checking intermediate results, just to be sure. Eventually, he is happy with the results an archives the script.\n",
    ">\n",
    "> At some later time, Bob is faced with a similar but slightly different programming task, so he copies the old script and adapts it to the new situation. While adapting, he realise that all this deep nesting of if/else statements that he did now makes it really difficult to understand the code. \"Maybe this is the reason why I overlooked this ...\", he thinks when he discovers another bug which, of course, sits inside some copy/paste code. He debugs the new and the old script and moves on. As new data comes in, the cycle continues, and after a while Bob opens his `python-scripts`folder and is not sure any more if he should use and adapt `script_foo_3.py` or `script_bar_2.py` for the new experiment. He realise that he is not even sure which script produced the results for that paper he submitted a few days ago ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I admit that this is rather a worst-case scenario, but Murphy's law says that it happend (though maybe not to Bob). Clearly, our unlucky programmer has to go through much more pain than would have been neccessary, if he had structured his code better."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Bob's starting problem was perhaps that he viewed his code as a *list of instructions*, and not as a *set of building blocks*. Building blocks in Python are functions, classes, and modules. These building blocks are reuseable, a list of instructions is not, at least not without violating the DRY principle ..."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### DRY\n",
    "\n",
    "DRY stands for [&rarr; \"*don't repeat yourself\"*](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself): Do not copy and paste your code and do not write redundant code. If you do, you will need to keep track of all these redundancies and this will waste your cognitive capacities. It will be easy to overlook some parts of your code when you update it. Also, you will eventually have to write more code.\n",
    "\n",
    "My concrete advise for our (somewhat special) case of data evaluation centered around Jupyter is:\n",
    "\n",
    "If you find yourself copying and pasting a code cell more than once, take the code and put it inside a function. If you want to use the code accross several notebooks, put it inside a module. If you want to use the code accross several projects, put it in an installable package. If that package has a version number and its own repository, it will be easy to reconstruct what version of the code was used for what data, and to keep the data evaluation repeatible."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### One function ~~to rule them all~~ for each job\n",
    "\n",
    "If you have organized your code into modules or packages you have gained a lot reusability: Now you can simply import whatever building block you need to write more sophisticated programs. However, this can only work if your building blocks are not too specialized.\n",
    "A function that does many things at once is a bit like the script that Bob wrote: whenever the boundary conditions change a bit, you have to adapt and rewrite it.\n",
    "\n",
    "A function should thus do not more than one thing. If it does, it should be composed from other functions but not implement the single steps itself. Simple functions with well defined tasks are not only more reusable, they are also easier to test, name, and document.\n",
    "If you find yourself naming a function and thinking that you should include an *and* in the name, think about splitting it up. If you have functions that do very similar things, it may be that there is some abstract pattern that causes the redundancy that you can split off."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Cyclomatic complexity\n",
    "\n",
    "These big words denote a quantitave measure of how many paths there are though your code.\n",
    "The number of paths scales exponentially with nested if/else clauses. Deep nesting makes it very difficult to read through the code and keeping in mind what conditions are met at what point, which makes it harder follow what is happening or where a problem may occur. Try to avoid nesting. This may be achieved by putting the code in the if/else clauses into separate functions. For example, this code:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "if filepath.endswith('.txt'):\n",
    "    with open(filepath, 'r') as f:\n",
    "        raw_text = f.read()\n",
    "    if 'some indicator' in raw_text:\n",
    "        ...\n",
    "        something = ...\n",
    "    else:\n",
    "        ...\n",
    "        something = ...\n",
    "elif filepath.endswith('.json'):\n",
    "    with open(filepath, 'r') as f:\n",
    "        contents = json.load(f)\n",
    "    if 'some indicator' in contents.keys():\n",
    "        ...\n",
    "        something = ...\n",
    "    else:\n",
    "        ...\n",
    "        something = ...\n",
    "else:\n",
    "    raise ValueError(\n",
    "        f\"File type of {os.path.basename(filepath)} not supported.\")\n",
    "```\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "is much less readable than this code:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "if filename.endswith('.txt'):\n",
    "    something = load_from_text(filename)\n",
    "elif filename.endswith('.json'):\n",
    "    something = load_from_json(filename)\n",
    "else:\n",
    "    raise ValueError(\n",
    "        f\"File type of {os.path.basename(filepath)} not supported.\")\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### State, pure functions, testability\n",
    "\n",
    "Another thing to consider when writing functions is whether or not the function depends (or should depend) on some kind of state. State may be, for example, a file that is present on the hard drive, a connection that is established (or not established) to a database, whether or not something is displayed on the screen, or the value of some global variable.\n",
    "\n",
    "If a function depends on things like these, you have to keep track of them when thinking about how the function behaves. If you want to test such a function, you must prepare the correct conditions for each possible state. The opposite of such *impure* functions are *pure* functions, which, given a certain input, will always return the same output, much like mathematical functions, and which do not have any *side effects*, such as writing data to disc or showing something on the screen. Pure functions are easier to test than impure functions and it helps to separate pure from impure behavior.\n",
    "\n",
    "If many functions neccessarily depend on the same state or input values, it may be a good idea to combine them into a class and have them share the state via instance variables:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "```python\n",
    "class MyClass:\n",
    "    \n",
    "    def __init__(self, variable_1, variable_2, variable_3):\n",
    "\n",
    "        self.instance_variable_1 = variable_1\n",
    "        self.instance_variable_2 = variable_2\n",
    "        self.instance_variable_3 = variable_3\n",
    "        \n",
    "    def method_1(self):\n",
    "\n",
    "        # do something with self.instance_variable_1,\n",
    "        #                                      ..._2,\n",
    "        #                                      ..._3\n",
    "        \n",
    "    def method_2(self):\n",
    "\n",
    "        # do something else with self.instance_variable_1,\n",
    "        #                                           ..._2,\n",
    "        #                                           ..._3\n",
    "        \n",
    "    def method_3(self):\n",
    "\n",
    "        # do yet another thing with self.instance_variable_1,\n",
    "        #                                              ..._2,\n",
    "        #                                              ..._3\n",
    "        \n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Sorting into modules"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Keep your code tidy by not throwing everything into a single module (.py file) but organizing it into separate modules that serve specific purposes. For example:\n",
    "\n",
    "|                    |                                         |\n",
    "|--------------------|-----------------------------------------|\n",
    "| `evaluation.py`    | data processing functions               |\n",
    "| `io.py`            | input/output: reading and writing files |\n",
    "| `preprocessing.py` | functions to tidy raw data              |\n",
    "| `utils.py`         | simple helper functions                 |\n",
    "| `visualization.py` | plotting functions                      |\n",
    "\n",
    "In each module, you can sort the code by importance (most important at the top) while trying to keep things that are related together.\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Using modules as command line tools\n",
    "\n",
    "Sometimes you may want to use a script directly from the command line and apply it to different files with different input options. Then it might be especially tempting to copy and paste the entire file and change some of the variables (violating the DRY principle), or to keep one file, change the variables and have Git indicate a modification all the time (I am guilty of doing this).\n",
    "\n",
    "A better way of making use of your code from the command line is to implement an actual command line interface (CLI). Python's standard library includes the module `argparse` to help you with that. Let's first look at a rather inefficient implementation and then how `argparse` removes much of the struggle."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Doing it the hard way\n",
    "\n",
    "As you know, you can run a Python script from the command line like so:\n",
    "\n",
    "```bash\n",
    "python3 my_script.py\n",
    "```\n",
    "\n",
    "If you want to manipulate the behavior of your script from the command line, you have to pass arguments:\n",
    "\n",
    "```bash\n",
    "python3 my_script.py argument_1 argument_2\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "In Python, all command line arguments (including the script's name) are available from the variable `sys.argv`.\n",
    "If you have a Python file with nothing more than:\n",
    "\n",
    "```python\n",
    "# my_script.py\n",
    "import sys\n",
    "\n",
    "print(sys.argv)\n",
    "```\n",
    "\n",
    "invoking it will display what arguments you provided:\n",
    "\n",
    "```bash\n",
    "python3 my_script.py foo bar\n",
    "\n",
    "['my_script.py', 'foo', 'bar']\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "I included such a minimal script in the [examples folder](examples/command-line-interface/minimal_sys_argv.py). You can try it here:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "python3 examples/command-line-interface/minimal_sys_argv.py this is a test"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now it is obvious how to implement a command line interface:\n",
    "Just take the input from `sys.argv` and let the program react to that. How hard can it be?\n",
    "\n",
    "In [examples/command-line-interface/greet_sys_argv.py](examples/command-line-interface/greet_sys_argv.py) I implemented a small program that greets the user. You have to put in your first name, last name, and, optionally, the flag `-f` or `..friendly` to change the tone of the greeting."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "python3 examples/command-line-interface/greet_sys_argv.py Monty Python\n",
    "python3 examples/command-line-interface/greet_sys_argv.py Monty Python -f\n",
    "python3 examples/command-line-interface/greet_sys_argv.py Monty Python -foo\n",
    "python3 examples/command-line-interface/greet_sys_argv.py Monty"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "python3 examples/command-line-interface/greet_sys_argv.py --help"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "You can see that even though this seems like a fairly simple program (we really care only for lines 56 to 59), we already have to account for a lot of different scenarios: Did the user pass a wrong argument? To many? To few? Does the user need help? To account for all of these cases, lots of if-else statements are required, increasing the complexity of the code. If we change the code, we also have to change the docstring of the module. Not very DRY."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "#### Using argparse\n",
    "\n",
    "In [examples/command-line-interface/greet_argparse.py](examples/command-line-interface/greet_argparse.py) I implemented the same program, this time using the module `argparse` to communicate between the command line and Python. Just by looking at it, you can see that the code is much more concise. This is what is happening:"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `argparse` module provides us with the class `ArgumentParser` which handles parsing the arugments from the command line, displaying error messages, and generating documentation. We only have to tell it what kind of arguments we want and what properties these arguments should have by calling its `add_argument()` method. Calling its `parse_args()` method will make it take the arguments from `sys.argv`, react approrpiately and pass on the values if everything is fine. In our main function, we only have to `get_args()` and access them via `args.argument_name`, instead of worrying about all the different usage scenarios.\n",
    "\n",
    "This implementation is not only better from the programmers point of view, the user also gets a better interface with more helpful error messages:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%bash --no-raise-error\n",
    "\n",
    "python3 examples/command-line-interface/greet_argparse.py Monty Python -f\n",
    "python3 examples/command-line-interface/greet_argparse.py Monty Python -foo\n",
    "python3 examples/command-line-interface/greet_argparse.py Monty"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "For simple applications, it is enough to understand how to use the `add_argument()` function, so here is a quick rundown:\n",
    "\n",
    "- the first *function* argument (of the function `add_argument`, not your script!) is the name of the *command line* argument; this defines how you access the value in the parsed command line arguments\n",
    "- a simple name without a dash (`-`) indicates a positional argument which the user has to type at the correct position\n",
    "- names starting with a single dash are optional arguments with arbitrary order; they are followed by a long name which the user can use synonymously\n",
    "- with the keyword argument `type` you can enforce a datatype\n",
    "- the keyword `action` can do many things, including invoking arbitrary functions; here it is used to make `-f` a true/false flag: if `-f` or `--friendly` is put in by the user, the associated value in Python is set to `True`, if not it is set to the default value `False` (provided by the `default` keyword argument)\n",
    "- the keyword argument `help` defines what is shown in the documentation (displayed when using the flag `-h` or `--help` on the command line)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "There is much more that you can do with `argparse`, for example, defining mutual exclusive arguments, passing in arguments from files, and so on. If you want to learn more, you find a [&rarr; tutorial](https://docs.python.org/3/howto/argparse.html) and the [&rarr; reference guide](https://docs.python.org/3/library/argparse.html) in the official Python documentation."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "<a id=\"excs\"></a>\n",
    "#### Exercise\n",
    "\n",
    "Modify `greet_argparse.py` such that the user can choose how often they want to be greeted.\n",
    "Test your program and see how it reacts on different inputs.\n",
    "What happens when you ask it to repeat the greeting \"four\" times (written as a word)?\n",
    "\n",
    "Solution: [examples/command-line-interface/greet_often_argparse.py](examples/command-line-interface/greet_often_argparse.py)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Making an installable package\n",
    "\n",
    "In the examples folder, you will find the package \"fizzbuzz\", which I prepared to show how to make a package pip-installable.\n",
    "The main ingredient is a [`setup.py` file](examples/fizzbuzz/setup.py), which can be understood by pip.\n",
    "I adapted the file from the official [&rarr; sample project](https://github.com/pypa/sampleproject) from the Python Packaging Authority to make it as minimalistic as possible.\n",
    "As you can see, the main thing that happens in `setup.py` is that we provide meta data like package name, version, and author by feeding it into the `setup()` function that is part of the `setuptools` library (part of the Python standard library)."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The `packages` keyword of the `setup()` function specifies which subfolders should be included when installing the package. The `find_packages()` function, without further arguments, includes all subfolders, which is fine in our simple example.\n",
    "Other important keywords are `python_requires`, which tells which Python versions are supported, and `install_requires`, which lists the dependencies of the projects  (e.g. `['scipy==1.1.0', 'bokeh>=1.0']`)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Other files which are included in the installation directory are a `README.md`, `LICENSE.txt`, and `MANIFEST.in`. The latter file specifies which files should be included when making a source distribution, which is not so relevant for us if we only want to share code between our own projects."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&uarr; back to TOC](#tableofcontents)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&larr; back to index.md](index.md)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "[&rarr; next notebook: unittesting.ipynb](unittesting.ipynb)"
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "jupyter-course",
   "language": "python",
   "name": "jupyter-course"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.5"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
