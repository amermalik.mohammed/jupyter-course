Here you find an outline of the main course and links to navigate the course material.

# Part I: Software development tools

- [environments.ipynb](environments.ipynb)
  - Why dedicated environments?
  - Creating dedicated environments
  - Creating a `requirements.txt` file
  - Registering environments with Jupyter
  - Workflow for creating new projects
  - Understanding version numbers
- [version-control.ipynb](version-control.ipynb)
  - What is version control and why bother?
  - Tracking, viewing, and reverting changes with [&rarr; Git](https://git-scm.com)
  - Syncing with online repository [&rarr; Gitlab](https://gitlab.gwdg.de)
  - [&rarr; VS Code](https://code.visualstudio.com/) editor integration
  - Diffing notebooks: [&rarr; nbdime](https://github.com/jupyter/nbdime)


---


- [style_1.ipynb](style_1.ipynb)
  - Code formatting: [&rarr; PEP8](https://www.python.org/dev/peps/pep-0008/)
  - Code analysis: linters and linter editor integration
- [style_2.ipynb](style_2.ipynb)
  - Naming recommendations
  - Writing documentation
  - Commenting code
- [style_3.ipynb](style_3.ipynb)
  - Code structure
  - Writing a command line interface
  - Making a package installable


---


- [unittesting.ipynb](unittesting.ipynb)
  - Assertions
  - Unittest module
  - Mocking

# Part II: Scientific Python Libraries

Data evaluation with [&rarr; Pandas](https://pandas.pydata.org/):

- [tabular-data_1.ipynb](tabular-data_1.ipynb)
  - Pandas dataframes
  - Reading CSV files
  - Handeling missing data
  - Data transformations
  - Combining data from different sources
- [tabular-data_2.ipynb](tabular-data_2.ipynb)
  - Selecting data and querying a dataframe
  - Statistical analysis
  - Plotting


---


Plotting with [&rarr; HoloViews](https://http://holoviews.org/):

- [holoviews_basic.ipynb](holoviews_basic.ipynb)
  - HoloViews core concepts
  - Elements, metadata, options
  - Output and backends
- [holoviews_nD-data.ipynb](holoviews_nD-data.ipynb)
  - Visualizing multi-dimensional data
  - Advanced interactivity


---


A more low-level way of interactive plotting (Bokeh and Ipywidgets) is described in [plotting_1.ipynb](plotting_1.ipynb), [plotting_2.ipynb](plotting_2.ipynb), and [plotting_3.ipynb](plotting_3.ipynb).
These notebooks were part of the previous course but will not be included in the "live action" this time.
They do not fully (but mostly) work with the new package versions that became available since 2019.

You are welcome to have a look on your own. The part about widgets ([plotting_2.ipynb](plotting_2.ipynb)) may be helpful even if you use mainly HoloViews for plotting.

