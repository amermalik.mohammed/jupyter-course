"""
Playground to demonstrate linter messages.
"""

# lines should be shorter than 80 characters

LONG = "The linter will complain if a line is longer than 80 characters, like this one."


# Function and variable names should use only small letters and distinguish
# words by underscores (snake_case)

def add_values(x, y):  # yes
    return x + y


def AddValues(x, y):   # no
    return x + y


def get_twice_the_foo():
    my_variable = 'foo'  # yes
    my_variable_2 = 'foo'   # no
    return (my_variable, myVariable)


# Names for classes should use capital letters and CamelCase

class FastCar(object):  # yes
    pass


class my_class(object):  # no
    pass


# global variables (constants) should be UPPERCASE

offset = 1
OFFSET = 1


# imports should be placed at the top of the module
# per line, only one module should be imported
# star imports should be avoided

import os  # yes (if at top of the file)
import sys

import os, sys  # no

from json import *  # no


# binary operators should be surrounded by whitespace

A = 1  # yes
B=2    # no

if A == 1:  # yes
    pass

if B!=2:    # no
    pass


# keyword arguments in function definitions are an exception:

def increment(a, b=1):    # yes
    return a + b


def decrement(a, b = 1):  # no
    return a - b


# listed objects should be separated by a comma + space

C = [1, 2, 3]    # yes
D = [1,2,3]      # no
E = [1 , 2 , 3]  # no


# if-else (try-except) statements should not be put in a single line

def is_answer(number):  # yes (in terms of formatting)
    if number == 42:
        return True
    else:
        return False


def is_answer_2(number):  # no
    if number == 42: return True
    else: return False


def is_answer_3(number):  # better
    return number == 42


# function arguments should be algined properly:

def do_something_complex_which_requires_many_arguments_1(  # yes
        argument_1, argument_2, argument_3, argument_4):
    return (argument_1 + argument_2, argument_3 + argument_4)


def do_something_complex_which_requires_many_arguments_2(argument_1,  # yes
                                                         argument_2,
                                                         argument_3,
                                                         argument_4):
    return (argument_1 + argument_2, argument_3 + argument_4)


def do_something_complex_which_requires_many_arguments_3(argument_1,  # no
        argument_2, argument_3, argument_4):
    return (argument_1 + argument_2, argument_3 + argument_4)


def do_something_complex_which_requires_many_arguments_4(  # no
    argument_1, argument_2, argument_3, argument_4):
    return (argument_1 + argument_2, argument_3 + argument_4)


# complex nesting should be avoided

def make_complex_decision_1(foo, bar, baz, qux):  # yes
    return all([foo, bar, baz, qux])


def make_complex_decision_2(foo, bar, baz, qux):  # no
    if foo and bar and baz and qux:
        return True
    else:
        return False


def make_complex_decision_3(foo, bar, baz, qux):  # no no no
    if foo:
        if bar:
            if baz:
                if qux:
                    return True
    return False
