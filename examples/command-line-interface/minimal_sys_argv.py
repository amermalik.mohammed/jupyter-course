"""
minimal_sys_argv.py

A minimal demonstration how to access command line arguments in Python
using sys.argv. Echos arbitrary arguments.
"""

import sys

print(sys.argv)
