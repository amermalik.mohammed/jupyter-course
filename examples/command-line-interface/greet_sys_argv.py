"""
usage: greet_sys_argv.py [-h] [-f] first_name last_name

greet_sys_argv.py

A simple demonstration of parsing arguments from the
command line to Python using sys.argv.

positional arguments:
  first_name      your first name
  last_name       your last name

optional arguments:
  -h, --help      show this help message and exit
  -f, --friendly
"""

import sys


def print_not_understood():
    print("error: input arguments not understood, "
          "use -h or --help to display documentation.")


def main():
    args_list = sys.argv[1:]  # the first argument is the script's file name

    # display help, if requested by user
    if '-h' in args_list or '--help' in args_list:
        print(__doc__)
        return

    # parse input arguments
    if len(args_list) == 2:
        first_name, last_name = args_list
        greeting_flag = None
    elif len(args_list) == 3:
        first_name, last_name, greeting_flag = args_list
    else:
        print_not_understood()
        return

    # parse greeting option
    # (could be incorporated in parsing of arguments, but this would lead to
    #  nesting the if-else statements)
    if greeting_flag is None:
        greet_friendly = False
    elif greeting_flag in ['-f', '--friendly']:
        greet_friendly = True
    else:
        print_not_understood()
        return

    # greet user
    if greet_friendly:
        print(f"Hello, {first_name} {last_name}! How are you?")
    else:
        print(f"Hello, {first_name} {last_name}.")


if __name__ == '__main__':
    main()
